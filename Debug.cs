using System;

public class Debug
{
    public static void LogGreen(object logMsg)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine(logMsg);
        Console.ForegroundColor = ConsoleColor.White;
    }

    public static void LogDarkGreen(object logMsg)
    {
        Console.ForegroundColor = ConsoleColor.DarkGreen;
        Console.WriteLine(logMsg);
        Console.ForegroundColor = ConsoleColor.White;
    }

    public static void LogYellow(object waringMsg)
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine(waringMsg);
        Console.ForegroundColor = ConsoleColor.White;
    }

    public static void LogError(object errorMsg)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(errorMsg);
        Console.ForegroundColor = ConsoleColor.White;
    }
}
