﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigGenerator
{
    /// <summary>
    /// 文件记录配置
    /// </summary>
    public class FileRecordConfig
    {
        /// <summary>
        /// 所有的Xlsx文件
        /// </summary>
        public Dictionary<string, FileRecord> FileRecordDict { get; set; }

        public FileRecordConfig()
        {
            FileRecordDict = new Dictionary<string, FileRecord>();
        }
    }

    /// <summary>
    /// 单条文件记录 用于生成Josn文件
    /// </summary>
    public class FileRecord
    {
        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
}
