﻿using System;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

namespace ConfigGenerator
{
    /// <summary>
    /// 配置表生成枚举
    /// </summary>
    public enum EGenType
    {
        /// <summary>
        /// 全部生成
        /// </summary>
        All = 0,
        /// <summary>
        /// 仅重新生成发生变化的配置表，包括新建的Excel配置
        /// 识别方式依赖于文件的修改时间
        /// </summary>
        OnlyChange = 1,
        /// <summary>
        /// 选中的文件
        /// </summary>
        Select = 2,
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            ConfigGenerator gen = new ConfigGenerator();

            if (args.Length >= 5)
            {
                EGenType type = (EGenType)Convert.ToInt32(args[0]);
                string originExcelDir = args[1];
                string clientXmlConfigDir = args[2];
                string serverXmlConfigDir = args[3];
                string clientLangPackeagePath = args[4];
                string serverLangPackeagePath = args[5];

                gen.GeneratorCfg(type, originExcelDir, clientXmlConfigDir, serverXmlConfigDir, clientLangPackeagePath, serverLangPackeagePath);
            }
            else
            {
                gen.GeneratorCfg(EGenType.All, ".\\Config\\ConfigExcel", ".\\Config\\ClientXml", ".\\Config\\ServerXml", ".\\Config\\ClientXml\\LangPackageCfg.xml", ".\\Config\\ServerXml\\LangPackageCfg.xml");
            }

            Console.ReadLine();
        }
    }
}

