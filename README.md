# ConfigGenerator

Excel文件转xml配置文件的生成工具，采用C#编写，引用NPOI库。该工具仅是一个C#控制台程序。

![物品表Excel](https://blog-1258865037.cos.ap-chengdu.myqcloud.com/xml-config-generator/image-20221214105610773.png)

![生成过程](https://blog-1258865037.cos.ap-chengdu.myqcloud.com/xml-config-generator/image-20221214110755950.png)

![物品表Xml](https://blog-1258865037.cos.ap-chengdu.myqcloud.com/xml-config-generator/image-20221214105859614.png)

![多语言表](https://blog-1258865037.cos.ap-chengdu.myqcloud.com/xml-config-generator/image-20221214105934847.png)

![发布版截图](https://blog-1258865037.cos.ap-chengdu.myqcloud.com/xml-config-generator/image-20221214110030269.png)

## 前置条件

- `.Net`环境



## 使用说明

1. 该工具会识别后缀名为`.xlsx`的文件，读取符合名称格式的`sheet`表，不符合名称格式的`sheet`表会被忽略，一个`.xlsx`中可以包含多个符合格式的表，均可生成`xml`文件。名称格式为：`xxx|yyy`，例如：`示例表|Example`

2. 该工具支持大部分`C#`基础类型的检查，未包含的类型会识别为错误配置，其生成将被跳过，有需要可自行修改源代码。支持的类型：`string`、`string[]`(`string`和`string[]`类型无条件支持检查通过)、`byte`、`byte[]`、`short`、`short[]`、`ushort`、`ushort[]`、`int`、`int[]`、`uint[]`、`uint[]`、`long`、`long[]`、`ulong`、`ulong[]`、`float`、`float[]`、`double`、`double[]`

3. 标识为数组格式的类型，需使用逗号`,`分隔开

6.  `Excel`文件中前4行被固定使用：
    
    - 第一行表示字段需要的端(`s`表示服务端`server`，`c`表示客户端`client`)
    - 第二行表示字段类型
    - 第三行表示字段名
    - 第四行表示注释，仅用于`Excel`文件中参考
    
5.  使用`#`作为起始的文本，将会识别为多语言表内容，会单独生成为`LangPackageCfg.xml`

6.  配置生成使用批处理的方式，批处理有中须填入**7**个参数

    - 参数1：生成类型，0表示全部生成、1表示仅生成修改的表、2表示生成指定的表
    - 参数2：`Excel`文件路径
    - 参数3：客户端`Xml`输出路径，用`_`则代表不生成客户端`Xml`
    - 参数4：服务端`Xml`输出路径，用`_`则代表不生成服务端`Xml`
    - 参数5：客户端多语言`Xml`输出路径，用`_`则代表不生成客户端多语言`Xml`
    - 参数6：服务端多语言`Xml`输出路径，用`_`则代表不生成服务端多语言`Xml`

    ```bash
    //批处理文件格式
    dotnet .\ConfigGenerator.dll 参数1 参数2 参数3 参数4 参数5 参数6
    pause
    
    //客户端表全部生成批处理示例
    dotnet .\ConfigGenerator.dll 0 .\Config\ConfigExcel .\Config\ClientXml _ .\Config\ClientXml\LangPackageCfg.xml _
    pause
    ```
